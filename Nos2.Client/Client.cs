﻿using System;
using System.Diagnostics;
using System.Net.Sockets;

namespace Nos2.Client
{
    internal class Client : IDisposable
    {
        private readonly Stopwatch _stopwatch;
        private readonly TcpClient _tcp;
        private readonly NetworkStream _stream;

        public Client(ClientOptions options)
        {
            _stopwatch = new Stopwatch();
            _tcp = new TcpClient(options.Server, options.Port)
            {
                NoDelay = true,
                SendTimeout = options.RetransmissionTimeout,
                SendBufferSize = options.PacketSize,
            };
            _stream = _tcp.GetStream();
            WriteHeader(options);
        }

        private void WriteHeader(ClientOptions options)
        {
            var header = new byte[8];
            BitConverter.GetBytes(options.PacketSize).CopyTo(header, 0);
            BitConverter.GetBytes(options.DataSize).CopyTo(header, 4);
            _stream.Write(header, 0, header.Length);
        }

        public double Write(byte[] buffer)
        {
            _stopwatch.Restart();
            _stream.Write(buffer);
            _stopwatch.Stop();
            return _stopwatch.Elapsed.TotalMilliseconds;
        }

        public void Dispose()
        {
            _tcp.Dispose();
        }
    }
}
