﻿using Nos2.Common;
using System;
using System.Diagnostics;

namespace Nos2.Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var options = Options.ConfigureOptions<ClientOptions>(args, "client.json");
            var partialPacketSize = options.DataSize % options.PacketSize;
            if (partialPacketSize != 0)
            {
                Console.WriteLine("[WARN] Data/packet size non-zero remainder");
            }
            Console.WriteLine($"Server address: {options.Server}:{options.Port}");
            Console.WriteLine($"Packet size: {options.PacketSize} bytes");
            Console.WriteLine($"Timeout: {options.RetransmissionTimeout} ms");

            var fullPackets = options.DataSize / options.PacketSize;
            var buffer = new byte[options.PacketSize];
            new Random().NextBytes(buffer);
            var watch = new Stopwatch();
            watch.Start();
            using (var client = new Client(options))
            {
                Console.CancelKeyPress += (s, a) => client.Dispose();
                Console.WriteLine("Connection established");
                Console.WriteLine($"Exchanging {options.PacketSize}*{fullPackets}+{partialPacketSize} bytes");
                for (var packet = 0; packet < fullPackets; ++packet)
                {
                    var time = client.Write(buffer);
                    Console.WriteLine($"Packet {packet}: {time} ms");
                }
                if (partialPacketSize != 0)
                {
                    var time = client.Write(buffer);
                    Console.WriteLine($"Packet {fullPackets}: {time} ms");
                }
            }
            watch.Stop();
            Console.WriteLine($"Overall: {watch.Elapsed.TotalMilliseconds} ms");
        }
    }
}
