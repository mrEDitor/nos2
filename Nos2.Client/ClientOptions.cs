﻿using Nos2.Common;
using System.ComponentModel.DataAnnotations;

namespace Nos2.Client
{
    internal class ClientOptions
    {
        [Required]
        public string Server { get; set; }

        [PositiveInt]
        public int Port { get; set; }

        [PositiveInt]
        public int DataSize { get; set; }

        [PositiveInt]
        public int PacketSize { get; set; }

        [PositiveInt]
        public int RetransmissionTimeout { get; set; }
    }
}
