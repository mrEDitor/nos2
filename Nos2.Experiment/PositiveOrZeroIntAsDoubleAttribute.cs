﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Nos2.Experiment
{
    internal class PositiveOrZeroIntAsDoubleAttribute : ValidationAttribute
    {
        private const double EPS = 1e-6;

        public PositiveOrZeroIntAsDoubleAttribute()
        {
            ErrorMessage = "Value must be positive or zero integer.";
        }

        public override bool IsValid(object value) =>
            value == null 
            || Math.Abs((double)value - Convert.ToInt32(value)) <= EPS && Convert.ToInt32(value) + 0.1 >= 0;
    }
}
