﻿using Microsoft.Extensions.Options;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nos2.Experiment
{
    internal class AtLeastOneElementAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var array = value as Array;
            if (array == null)
            {
                ErrorMessage = "Array expected.";
                return false;
            }
            if (array.Length == 0)
            {
                ErrorMessage = "Array must contain at least one element.";
                return false;
            }

            var validate = ValidateOptionByDataAnnotations(value.GetType().GetElementType());
            ErrorMessage = string.Empty;
            for (var i = 0; i < array.Length; ++i)
            {
                var result = validate(array.GetValue(i));
                if (result.Failed)
                {
                    ErrorMessage += $"element #{i}: [{result.FailureMessage}]\n";
                }
            }
            return ErrorMessage == string.Empty;
        }

        private static Func<object, ValidateOptionsResult> ValidateOptionByDataAnnotations(Type objectType)
        {
            var validatorType = typeof(DataAnnotationValidateOptions<>).MakeGenericType(objectType);
            var validatorConstructor = validatorType.GetConstructor(new Type[] { typeof(string) });
            var validatorMethod = validatorType.GetMethod(
                nameof(DataAnnotationValidateOptions<object>.Validate),
                new Type[] { typeof(string), objectType }
            );
            var validator = validatorConstructor.Invoke(new object[] { null });
            return obj => (ValidateOptionsResult)validatorMethod.Invoke(validator, new object[] { null, obj });
        }
    }
}