﻿using Nos2.Common;
using System.ComponentModel.DataAnnotations;

namespace Nos2.Experiment
{
    internal class ExperimentOptions
    {
        [Required]
        public string ClientExecutable { get; set; }

		public bool Quiet { get; set; }

		[PositiveInt]
        public int MaxThreads { get; set; } = 1;

        [Required]
        public string Server { get; set; }

        [PositiveInt]
        public int Port { get; set; }

        [AtLeastOneElement]
        public IntRange[] DataSize { get; set; }

        [AtLeastOneElement]
        public IntRange[] PacketSize { get; set; }

        [AtLeastOneElement]
        public IntRange[] RetransmissionTimeout { get; set; }
    }
}