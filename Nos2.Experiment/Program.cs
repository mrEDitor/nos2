﻿using Nos2.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Nos2.Experiment
{
	public class Program
	{
		public static async Task Main(string[] args)
		{
			var options = Options.ConfigureOptions<ExperimentOptions>(args, "experiment.json");
			var varableTestResult = 0
				+ Math.Min(options.RetransmissionTimeout.SelectMany(AsIs).Count(), 2)
				+ Math.Min(options.DataSize.SelectMany(AsIs).Count(), 2)
				+ Math.Min(options.PacketSize.SelectMany(AsIs).Count(), 2);
			if (varableTestResult > 4)
			{
				Console.WriteLine("Only one integer should contain varaible range.");
				return;
			}

			var results = DoExperiments(options);
			Console.WriteLine("timeout;packet, bytes;data, bytes;overall time;packet times");
			foreach (var to in options.RetransmissionTimeout.SelectMany(AsIs))
				foreach (var ps in options.PacketSize.SelectMany(AsIs))
					foreach (var ds in options.DataSize.SelectMany(AsIs))
					{
						var experimentData = new ExperimentData(to, ps, ds);
						Console.Write($"{experimentData.Timeout};{experimentData.PacketSize};{experimentData.DataSize};");
						var experimentResult = await results[experimentData];
						Console.Write($"{experimentResult.OverallTime};");
						foreach (var time in experimentResult.PacketTime)
						{
							Console.Write($"{time};");
						}
						Console.WriteLine();
					}

		}

		private static Dictionary<ExperimentData, Task<ExperimentResult>> DoExperiments(ExperimentOptions options)
		{
			using (var cancellationSource = new CancellationTokenSource())
			using (var semaphore = new SemaphoreSlim(options.MaxThreads))
			{
				var results = new Dictionary<ExperimentData, Task<ExperimentResult>>();
				Console.CancelKeyPress += (s, a) => cancellationSource.Cancel();
				foreach (var to in options.RetransmissionTimeout.SelectMany(AsIs))
					foreach (var ps in options.PacketSize.SelectMany(AsIs))
						foreach (var ds in options.DataSize.SelectMany(AsIs))
						{
							var experimentData = new ExperimentData(to, ps, ds);
							results[experimentData] = Experiment(experimentData, options, semaphore, cancellationSource.Token);
						}
				return results;
			}
		}

		private static async Task<ExperimentResult> Experiment(
			ExperimentData experimentData,
			ExperimentOptions options,
			SemaphoreSlim semaphore,
			CancellationToken cancellationToken
			)
		{
			if (!options.Quiet)
			{
				Console.WriteLine("Constructing experiment " + experimentData);
			}
			var process = new Process
			{
				StartInfo = new ProcessStartInfo
				{
					FileName = "dotnet",
					Arguments = string.Join(' ',
						options.ClientExecutable,
						$"--{nameof(options.Server)}={options.Server}",
						$"--{nameof(options.Port)}={options.Port}",
						$"--{nameof(options.DataSize)}={experimentData.DataSize}",
						$"--{nameof(options.PacketSize)}={experimentData.PacketSize}",
						$"--{nameof(options.RetransmissionTimeout)}={experimentData.Timeout}"
					),
					CreateNoWindow = true,
					RedirectStandardOutput = true,
				},
			};
			await semaphore.WaitAsync(cancellationToken);
			if (!options.Quiet)
			{
				Console.WriteLine("Starting experiment with " + experimentData);
			}
			var started = process.Start();
			var experimentResult = new ExperimentResult();
			while (true)
			{
				var line = process.StandardOutput.ReadLine();
				if (line == null) break;
				if (line.StartsWith("Overall:"))
				{
					experimentResult.OverallTime = GetTime(line);
				}
				else if (line.StartsWith($"Packet {experimentResult.PacketTime.Count}:"))
				{
					experimentResult.PacketTime.Add(GetTime(line));
				}
			}
			process.WaitForExit();
			semaphore.Release();
			if (!options.Quiet)
			{
				Console.WriteLine($"Experiment with {experimentData} done");
			}
			return experimentResult;
		}

		private static Regex timeRegex = new Regex(@"[^:]+: ([0-9.,]+) [a-z]+");
		private static double GetTime(string line) =>
			double.Parse(timeRegex.Match(line).Groups[1].Value);

		private static IEnumerable<T> AsIs<T>(IEnumerable<T> arg) => arg;

		private struct ExperimentData
		{
			public readonly int Timeout;
			public readonly int PacketSize;
			public readonly int DataSize;

			public ExperimentData(int to, int ps, int ds)
			{
				Timeout = to;
				PacketSize = ps;
				DataSize = ds;
			}

			public override string ToString() => base.ToString()
				+ $"[{nameof(Timeout)}={Timeout}, {nameof(PacketSize)}={PacketSize}, {nameof(DataSize)}={DataSize}]";
		}

		private class ExperimentResult
		{
			public double OverallTime { get; set; }
			public List<double> PacketTime { get; } = new List<double>();
		}
	}
}
