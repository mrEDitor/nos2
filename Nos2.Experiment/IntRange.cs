﻿using Nos2.Common;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Nos2.Experiment
{
    public class IntRange : IEnumerable<int>
    {
        public int Value
        {
            get => 0;
            set => From = To = value;
        }

        [PositiveOrZeroIntAsDouble]
        public double Steps
        {
            get => DivideOrOne(To - From, (double)Step);
            set => Step = (int)DivideOrOne(To - From, value);
        }

        private static double DivideOrOne(int value, double divisor) =>
            (divisor == 0) ? 1 : (value / divisor);

        private static int DivideOrOne(int value, int divisor) =>
            (divisor == 0) ? 1 : (value / divisor);

        [PositiveInt]
        public int From { get; set; }

        [PositiveInt]
        public int To { get; set; }

        private int _step = 1;
        public int Step
        {
            get => _step * DivideOrOne(To - From, Math.Abs(To - From));
            set => _step = Math.Abs(value);
        }

        public IEnumerator<int> GetEnumerator()
        {
            for (var i = From; i != To; i += Step)
            {
                yield return i;
            }
            yield return To;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
