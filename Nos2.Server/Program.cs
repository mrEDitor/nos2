﻿using Nos2.Common;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Nos2.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var options = Options.ConfigureOptions<ServerOptions>(args, "server.json");
            var tcp = new TcpListener(IPAddress.Any, options.Port);
            var cancellationSource = new CancellationTokenSource();
            Console.CancelKeyPress += (s, a) => cancellationSource.Cancel();
            Console.WriteLine($"Server port: {options.Port}");
            tcp.Start();
            using (cancellationSource.Token.Register(() => tcp.Stop()))
            {
                while (!cancellationSource.IsCancellationRequested)
                {
                    AcceptClientData(tcp.AcceptTcpClient());
                }
            }
        }

        private static async void AcceptClientData(TcpClient client)
        {
            var id = Guid.NewGuid();
            await Task.Yield();
            try
            {
                var stream = client.GetStream();
                var buffer = new byte[8];
                stream.Read(buffer, 0, 8);
                client.NoDelay = true;

                var read = 0;
                var packetSize = BitConverter.ToInt32(buffer, 0);
                var dataSize = BitConverter.ToInt32(buffer, 4);
                Console.WriteLine($"[{id}] Client: connected");
                Console.WriteLine($"[{id}] Packet size: {packetSize} bytes");
                Console.WriteLine($"[{id}] Data size: {dataSize} bytes");

                buffer = new byte[packetSize];
                while (read < dataSize)
                {
                    var packet = stream.Read(buffer);
                    read += packet;
                    Console.WriteLine($"[{id}] Received {packet} bytes ({100 * read / dataSize}%)");
                    if (packet == 0)
                    {
                        throw new IOException("Client disconnected early, dropping connection too");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"[{id}] {e}");
            }
            client.Dispose();
            Console.WriteLine($"[{id}] Disconnected");
        }
    }
}
