﻿using Nos2.Common;

namespace Nos2.Server
{
    internal class ServerOptions
    {
        [PositiveInt]
        public int Port { get; set; }
    }
}
